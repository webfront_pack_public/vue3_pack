FROM node:16
COPY .  ./vue3_project
RUN npm install --registry=https://registry.npm.taobao.org \ 
    && npm install \
    && npm run dev
    
CMD yarn start